package com.dragon.dragongo.objects;

/**
 * Created by Mothership on 09/08/2016.
 */
public class Knight {
    private String name;
    private int attack;
    private int armor;
    private int agility;
    private int endurance;

    public Knight(String name, int attack, int armor, int agility, int endurance) {
        this.setName(name);
        this.setAttack(attack);
        this.setArmor(armor);
        this.setAgility(agility);
        this.setEndurance(endurance);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getArmor() {
        return armor;
    }

    public void setArmor(int armor) {
        this.armor = armor;
    }

    public int getAgility() {
        return agility;
    }

    public void setAgility(int agility) {
        this.agility = agility;
    }

    public int getEndurance() {
        return endurance;
    }

    public void setEndurance(int endurance) {
        this.endurance = endurance;
    }
}
