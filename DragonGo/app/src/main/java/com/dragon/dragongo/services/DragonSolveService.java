
package com.dragon.dragongo.services;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.dragon.dragongo.CommonFunctions;
import com.dragon.dragongo.R;
import com.dragon.dragongo.objects.Dragon;
import com.dragon.dragongo.objects.Game;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import butterknife.Bind;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;


public class DragonSolveService {

    StringEntity entity = null;
    @Bind(R.id.wonAmount)
    TextView wonAmount;
    @Bind(R.id.lostAmount)
    TextView lostAmount;

    @Bind(R.id.editText)
    EditText selectedAmount;

    View rootView;
    String comingServiceCall = "http://www.dragonsofmugloar.com/api/game/";
    RequestParams params = new RequestParams();
    protected AsyncHttpClient client = new AsyncHttpClient();

    public DragonSolveService(View rootView, Dragon choosenDragon) {
        this.rootView = rootView;
        this.params = params;
        if (choosenDragon != null) {
            try {
                Gson gson = new Gson();
                entity = new StringEntity("{ \"dragon\":"+gson.toJson(choosenDragon)+"}" );
                /*
                  entity = new StringEntity("{ \"dragon\": {\"scaleThickness\": 1,  \"clawSharpness\": 5,\n" +
                        "        \"wingStrength\": 4,\n" +
                        "        \"fireBreath\": 10 } }");
                 */


            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        Game gameInfo = (Game) CommonFunctions.getObjectFromLocalDatabase("game", rootView.getContext());
        comingServiceCall = comingServiceCall + Integer.toString(gameInfo.getGameId()) + "/solution";
        ButterKnife.bind(this, rootView);
        mainServiceCall();

      /*  JSONObject jsonParams = new JSONObject();
        jsonParams.put("notes", "Test api support");
        StringEntity entity = new StringEntity(jsonParams.toString());
        client.post(context, restApiUrl, entity, "application/json",
                responseHandler);*/



    }

    public void mainServiceCall() {


        client.put(rootView.getContext(), comingServiceCall, entity, "application/json", new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String incomingJson = new String(responseBody);
                if (incomingJson != null && !incomingJson.isEmpty()) {
                    JSONObject result = null;

                    try {
                        result = new JSONObject(incomingJson);
                        if (result != null && !result.isNull("status")) {
                            String status = result.getString("status");
                            if (status.equalsIgnoreCase("Victory")) {
                                wonAmount.setText(Integer.toString(Integer.parseInt(wonAmount.getText().toString()) + 1));
                            } else if (status.equalsIgnoreCase("Defeat")) {
                                lostAmount.setText(Integer.toString(Integer.parseInt(lostAmount.getText().toString()) + 1));

                            } else {

                            }
                            int winnings = Integer.parseInt(wonAmount.getText().toString());
                            int lossings = Integer.parseInt(lostAmount.getText().toString());

                            if (winnings + lossings != Integer.parseInt(selectedAmount.getText().toString())) {
                                new DragonStartService(rootView);
                                CommonFunctions.createToast("Round Finish. Next Round Starts", rootView.getContext());
                            }

                        } else {
                            CommonFunctions.createToast("Some issue at Result Data", rootView.getContext());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String incomingJson = new String(responseBody);
                CommonFunctions.createToast("Some issue at Solve Service", rootView.getContext());
            }

        });
    }


}