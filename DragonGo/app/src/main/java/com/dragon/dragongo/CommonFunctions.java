package com.dragon.dragongo;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.dragon.dragongo.objects.Game;
import com.google.gson.Gson;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Created by Mothership on 09/08/2016.
 */
public class CommonFunctions extends AppCompatActivity {
    static private Gson gson = new Gson();



    static public void createToast(String information, Context incomingContext) {
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(incomingContext, information, duration);
        toast.show();
    }
    public static Document loadXMLFromString(String xml) throws Exception
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader(xml));
        return builder.parse(is);
    }
    static public Object getObjectFromLocalDatabase(String key,Context savedContext) {

        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(savedContext);
        String json = appSharedPrefs.getString(key, "");
        if (json.equalsIgnoreCase("")) {
            return null;
        }
        //Customer is the user data actually.
        if (key.equalsIgnoreCase("game")) {
            return gson.fromJson(json, Game.class);
        }
        return null;
    }
    static public boolean saveObjectToLocalDatabase(String key, Object value,Context savedContext) {

        String json = gson.toJson(value);
        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(savedContext);
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
        prefsEditor.putString(key, json);
        return prefsEditor.commit();
    }
}
