package com.dragon.dragongo.services;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.TextView;

import com.dragon.dragongo.CommonFunctions;
import com.dragon.dragongo.FighterAI;
import com.dragon.dragongo.R;
import com.dragon.dragongo.objects.Dragon;
import com.dragon.dragongo.objects.Game;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.text.MessageFormat;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import butterknife.Bind;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;


public class WeatherService {
    String comingServiceCall = "http://www.dragonsofmugloar.com/weather/api/report/";
    View rootView;
    Document incoming;
    Dragon dragon;

    @Bind(R.id.dragonInfo)
    TextView dragonInfo;
    FighterAI fighter = new FighterAI();
    protected AsyncHttpClient client = new AsyncHttpClient();

    public WeatherService(View rootView, int gameId) {
        this.rootView = rootView;
        comingServiceCall = comingServiceCall + Integer.toString(gameId);


        ButterKnife.bind(this, rootView);
        mainServiceCall();
    }


    public void mainServiceCall() {

        client.get(comingServiceCall, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String incomingJson = new String(responseBody);
                if (incomingJson != null && !incomingJson.isEmpty()) {
                    try {
                        incoming = CommonFunctions.loadXMLFromString(incomingJson);
                        if (incoming != null) {
                            //message
                            NodeList code = incoming.getElementsByTagName("code");
                            if (code == null || code.getLength() == 0) {

                                CommonFunctions.createToast("Some issue at Weather Data Retrieving", rootView.getContext());
                            } else {
                                //FUNDEFINEDG FOG   nmr normal    SRO storm   HVA heavy rain  T E Long Dry
                                Game gameInfo = (Game) CommonFunctions.getObjectFromLocalDatabase("game", rootView.getContext());

                                String codeInfo = ((Element) code.item(0)).getTextContent();
                                if (codeInfo.equalsIgnoreCase("FUNDEFINEDG")) {
                                    dragonInfo.setText(String.format("Weather Code:%s Meaning: Fog", codeInfo));
                                    fighter.calculate(dragonInfo, "Fog", gameInfo.getKnight(), rootView);
                                }
                                if (codeInfo.equalsIgnoreCase("NMR")) {
                                    dragonInfo.setText(String.format("Weather Code:%s Meaning: Normal", codeInfo));
                                    fighter.calculate(dragonInfo, "Normal", gameInfo.getKnight(), rootView);
                                }

                                if (codeInfo.equalsIgnoreCase("SRO")) {

                                    dragonInfo.setText(MessageFormat.format("Weather Code:{0} Meaning: Storm", codeInfo));
                                    fighter.calculate(dragonInfo, "Storm", gameInfo.getKnight(), rootView);
                                }
                                if (codeInfo.equalsIgnoreCase("HVA")) {
                                    dragonInfo.setText(MessageFormat.format("Weather Code:{0} Meaning: Heavy Rain", codeInfo));
                                    fighter.calculate(dragonInfo, "Heavy Rain", gameInfo.getKnight(), rootView);
                                }

                                if (codeInfo.equalsIgnoreCase("T E")) {
                                    dragonInfo.setText(String.format("Weather Code:%s Meaning: Long Dry", codeInfo));
                                    fighter.calculate(dragonInfo, "Long Dry", gameInfo.getKnight(), rootView);

                                }


                            }


                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    CommonFunctions.createToast("Some issue at Weather Info", rootView.getContext());
                }


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String incomingJson = new String(responseBody);
            }

        });
    }


}