package com.dragon.dragongo;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.dragon.dragongo.services.DragonStartService;

public class MainActivity extends CommonFunctions {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickLoserKnight(View v)
    {
        View rootView=findViewById(android.R.id.content);
        new DragonStartService(rootView);
    }
}
