package com.dragon.dragongo.objects;

/**
 * Created by Mothership on 09/08/2016.
 */
public class Dragon {
    private int scaleThickness;
    private int clawSharpness;
    private int wingStrength;
    private int fireBreath;

    public Dragon(int scaleThickness, int clawSharpness, int wingStrength, int fireBreath) {
        this.setScaleThickness(scaleThickness);
        this.setClawSharpness(clawSharpness);
        this.setWingStrength(wingStrength);
        this.setFireBreath(fireBreath);
    }


    public int getScaleThickness() {
        return scaleThickness;
    }

    public void setScaleThickness(int scaleThickness) {
        this.scaleThickness = scaleThickness;
    }

    public int getClawSharpness() {
        return clawSharpness;
    }

    public void setClawSharpness(int clawSharpness) {
        this.clawSharpness = clawSharpness;
    }

    public int getWingStrength() {
        return wingStrength;
    }

    public void setWingStrength(int wingStrength) {
        this.wingStrength = wingStrength;
    }

    public int getFireBreath() {
        return fireBreath;
    }

    public void setFireBreath(int fireBreath) {
        this.fireBreath = fireBreath;
    }
}
