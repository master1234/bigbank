package com.dragon.dragongo.services;

import android.view.View;
import android.widget.TextView;

import com.dragon.dragongo.CommonFunctions;
import com.dragon.dragongo.R;
import com.dragon.dragongo.objects.Game;
import com.dragon.dragongo.objects.Knight;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;


public class DragonStartService {


    View rootView;
    final String comingServiceCall="http://www.dragonsofmugloar.com/api/game";
    protected AsyncHttpClient client = new AsyncHttpClient();

    @Bind(R.id.loserKnight)
    TextView loserKnight;

    Game incomingGame;
    Knight registeredKnight;

    public DragonStartService(View rootView) {
        this.rootView=rootView;

        ButterKnife.bind(this, rootView);
        mainServiceCall();
    }

    public void mainServiceCall() {

        client.get(comingServiceCall, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String incomingJson = new String(responseBody);
                //Response retrieved.
                if(incomingJson!=null && !incomingJson.isEmpty()){
                    try {
                        //Changing int o the Json Array.
                        JSONObject gameObject = new JSONObject(incomingJson);
                        if(gameObject!=null ){
                            JSONObject knightObject = gameObject.getJSONObject("knight");
                            if(knightObject!=null ){
                                //Lets check if any data does not exist
                                if ( gameObject.isNull("gameId") || knightObject.isNull("name") ||
                                        knightObject.isNull("attack") || knightObject.isNull("armor") ||  knightObject.isNull("agility")
                                        ||  knightObject.isNull("endurance"))
                                {
                                    CommonFunctions.createToast("Missing data from the Server", rootView.getContext());
                                }else{
                                    registeredKnight= new Knight(knightObject.getString("name"),knightObject.getInt("attack"),
                                            knightObject.getInt("armor"),knightObject.getInt("agility"),knightObject.getInt("endurance"));
                                    incomingGame= new Game(gameObject.getInt("gameId"),registeredKnight);

                                    loserKnight.setText("Game Id:"+incomingGame.getGameId()+"\n Knight name:"+registeredKnight.getName()
                                            +"\n Agility:"+registeredKnight.getAgility()+"\n Armor:"+registeredKnight.getArmor()+
                                            "\n Attack:"+registeredKnight.getAttack()+ "\n Endurance:"+registeredKnight.getEndurance());
                                    CommonFunctions.saveObjectToLocalDatabase("game",incomingGame,rootView.getContext());

                                    new WeatherService(rootView,incomingGame.getGameId());





                                }
                            }else{
                                CommonFunctions.createToast("Some issue at Getting Knight Detail Info", rootView.getContext());
                            }
                        }else{
                            CommonFunctions.createToast("Some issue at Getting Game Info", rootView.getContext());
                        }





                    } catch (JSONException e) {
                        CommonFunctions.createToast("Some issue at Converting the Knight Data", rootView.getContext());
                    }
                    ;
                }else{
                    CommonFunctions.createToast("Some issue at Getting the Knight Data", rootView.getContext());
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String incomingJson = new String(responseBody);

            }

        });
    }


}