package com.dragon.dragongo;

import android.view.View;
import android.widget.TextView;

import com.dragon.dragongo.objects.Dragon;
import com.dragon.dragongo.objects.Game;
import com.dragon.dragongo.objects.Knight;
import com.dragon.dragongo.services.DragonSolveService;
import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Mothership on 09/08/2016.
 */
public class FighterAI {
    Dragon createdDragon;
    RequestParams params=new RequestParams();
    protected Gson gson = new Gson();
    private static final int[][] BRUTFORCED = {
            {5, 5, 5, 5}, {10, 4, 3, 3},
            {5, 5, 5, 5}, {10, 4, 3, 3},
            {8, 8, 4, 0}, {10, 6, 3, 1},
            {8, 7, 5, 0}, {10, 5, 4, 1},
            {8, 8, 3, 1}, {10, 6, 2, 2},
            {8, 7, 4, 1}, {10, 5, 4, 1},
            {8, 6, 5, 1}, {10, 4, 5, 1},
            {7, 7, 5, 1}, {9, 5, 4, 2},
            {7, 6, 6, 1}, {9, 4, 4, 3},
            {8, 8, 2, 2}, {10, 6, 3, 1},
            {8, 7, 3, 2}, {10, 5, 4, 1},
            {8, 6, 4, 2}, {10, 4, 3, 3},
            {7, 7, 4, 2}, {10, 5, 4, 1},
            {8, 5, 5, 2}, {10, 5, 4, 1},
            {7, 6, 5, 2}, {10, 4, 4, 2},
            {8, 6, 6, 0}, {10, 4, 4, 2},
            {7, 7, 6, 0}, {10, 4, 4, 2},
            {6, 6, 6, 2}, {10, 4, 3, 3},
            {8, 6, 3, 3}, {10, 4, 4, 2},
            {7, 7, 3, 3}, {10, 4, 4, 2},
            {8, 5, 4, 3}, {10, 4, 4, 2},
            {7, 6, 4, 3}, {9, 4, 5, 2},
            {7, 5, 5, 3}, {9, 5, 4, 2},
            {6, 6, 5, 3}, {8, 5, 4, 3},
            {8, 4, 4, 4}, {10, 4, 3, 3},
            {7, 5, 4, 4}, {10, 4, 3, 3},
            {6, 6, 4, 4}, {10, 4, 3, 3},
            {6, 5, 5, 4}, {10, 4, 3, 3}

    };
     public void calculate(TextView incomingText,String weatherInfo, Knight knight,View rootView){

        if(weatherInfo.equalsIgnoreCase("Storm")){
          /*  createdDragon=new Dragon(17,1,1,1);
            incomingText.setText(MessageFormat.format("{0}\n All man must die. Same as Dragons\n ", incomingText.getText()));
            incomingText.setText(MessageFormat.format
                    ("{0}scaleThickness:{1}\nclawSharpness:{2}\nwingStrength:{3}\nfireBreath:{4}",
                            incomingText.getText(), createdDragon.getScaleThickness(), createdDragon.getClawSharpness(),
                            createdDragon.getWingStrength(), createdDragon.getFireBreath()));
            String dragonInfo = gson.toJson(createdDragon);
            params.put("dragon",dragonInfo);*/
            new DragonSolveService(rootView,null);
        }
         if(weatherInfo.equalsIgnoreCase("Heavy Rain")){
             createdDragon=new Dragon(7,7,6,0);
             incomingText.setText(MessageFormat.format("{0}scaleThickness:{1}\nclawSharpness:{2}\nwingStrength:{3}\nfireBreath:{4}",
                     incomingText.getText(), createdDragon.getScaleThickness(), createdDragon.getClawSharpness(), createdDragon.getWingStrength(),
                     createdDragon.getFireBreath()));


             new DragonSolveService(rootView,createdDragon);
         }
         if(weatherInfo.equalsIgnoreCase("Long Dry")){
             createdDragon=new Dragon(5,5,7,3);
             incomingText.setText(MessageFormat.format("{0}scaleThickness:{1}\nclawSharpness:{2}\nwingStrength:{3}\nfireBreath:{4}",
                     incomingText.getText(), createdDragon.getScaleThickness(), createdDragon.getClawSharpness(), createdDragon.getWingStrength(),
                     createdDragon.getFireBreath()));

             new DragonSolveService(rootView,createdDragon);
         }
         if(weatherInfo.equalsIgnoreCase("Fog")){
             createdDragon=new Dragon(5,5,5,5);
             incomingText.setText(MessageFormat.format("{0}scaleThickness:{1}\nclawSharpness:{2}\nwingStrength:{3}\nfireBreath:{4}",
                     incomingText.getText(), createdDragon.getScaleThickness(), createdDragon.getClawSharpness(), createdDragon.getWingStrength(),
                     createdDragon.getFireBreath()));

             new DragonSolveService(rootView,createdDragon);
         }
         if(weatherInfo.equalsIgnoreCase("Normal")){
             final List<Integer> knightPower = Arrays.asList(knight.getAttack(),
                     knight.getArmor(), knight.getAgility(), knight.getEndurance());
             final Integer[] indexes = { 0, 1, 2, 3 };

             Arrays.sort(indexes, new Comparator<Integer>() {
                 @Override public int compare(final Integer o1, final Integer o2) {
                     return knightPower.get(o1).compareTo(knightPower.get(o2));
                 }
             });
             int biggestIndex = indexes[3];
             int secondIndex = indexes[2];
             int thirdIndex = indexes[1];
             int forthIndex = indexes[0];
             int[] dragonPower= new int[] {0, 0, 0, 0};
             for (int index = 0; index < BRUTFORCED.length; index += 2) {
                 if (knightPower.get(biggestIndex) == BRUTFORCED[index][0]
                         && knightPower.get(secondIndex) == BRUTFORCED[index][1]
                         && knightPower.get(thirdIndex) == BRUTFORCED[index][2]
                         && knightPower.get(forthIndex) == BRUTFORCED[index][3]) {
                     dragonPower[biggestIndex] = BRUTFORCED[index + 1][0];
                     dragonPower[secondIndex] = BRUTFORCED[index + 1][1];
                     dragonPower[thirdIndex] = BRUTFORCED[index + 1][2];
                     dragonPower[forthIndex] = BRUTFORCED[index + 1][3];
                     break;
                 }
             }

             createdDragon=new Dragon(dragonPower[0],dragonPower[1],dragonPower[2],dragonPower[3]);
             new DragonSolveService(rootView,createdDragon);
            /*
             incomingText.setText(MessageFormat.format("{0}scaleThickness:{1}\nclawSharpness:{2}\nwingStrength:{3}\nfireBreath:{4}",
                     incomingText.getText(), createdDragon.getScaleThickness(), createdDragon.getClawSharpness(), createdDragon.getWingStrength(),
                     createdDragon.getFireBreath()));

            */
         }
    }
}
