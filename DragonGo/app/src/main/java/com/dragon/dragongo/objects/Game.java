package com.dragon.dragongo.objects;

/**
 * Created by Mothership on 09/08/2016.
 */
public class Game {
    private int gameId;
    private Knight knight;

    public Game(int incomingId, Knight incomingKnight) {
        setGameId(incomingId);
        setKnight(incomingKnight);
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public Knight getKnight() {
        return knight;
    }

    public void setKnight(Knight knight) {
        this.knight = knight;
    }
}
